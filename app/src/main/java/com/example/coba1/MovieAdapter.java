package com.example.coba1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MovieAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<Movie> movies = new ArrayList<>();

    public void setMovies(ArrayList<Movie> movies) {this.movies = movies;}

    public MovieAdapter(Context context){
        this.context = context;
    }

    public int getCount(){
        return movies.size();
    }

    public Object getItem(int position){
        return movies.get(position);
    }

    public long getItemId(int position){
        return position;
    }

    public View getView(int position, View ConvertView, ViewGroup parent){
        View itemView = ConvertView;
        if(itemView == null){
            itemView = LayoutInflater.from(context).inflate(R.layout.activity_item, parent, false);
        }
        ViewHolder viewHolder = new ViewHolder(itemView);
        Movie mov = (Movie) getItem(position);
        viewHolder.bind(mov);
        return itemView;
    }

    private class ViewHolder{
        private TextView txtNama, txtTahun;
        private ImageView foto;

        ViewHolder(View view){
            txtNama = view.findViewById(R.id.txtNama);
            txtTahun = view.findViewById(R.id.txtTahun);
            foto = view.findViewById(R.id.fotoPahlawan);
        }
        void bind(Movie mov){
            txtNama.setText(mov.getNama());
            txtTahun.setText(mov.getTahun());
            foto.setImageResource(mov.getFoto());
        }
    }
}
