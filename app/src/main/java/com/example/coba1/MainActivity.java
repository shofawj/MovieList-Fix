package com.example.coba1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ListView;
import android.content.Intent;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private String[] dataname, datatahun;
    private TypedArray datafoto;
    private MovieAdapter adapter;
    private ArrayList<Movie> heroes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new MovieAdapter(this);
        ListView listView = findViewById(R.id.movielist);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int asd = position;
                Intent intent = new Intent(MainActivity.this, MovieDetail.class);
                intent.putExtra("asd",asd);
                startActivity(intent);
            }
        });
        prepare();
        addItem();


    }





    private void prepare(){
        dataname = getResources().getStringArray(R.array.movie);
        datatahun = getResources().getStringArray(R.array.movieThn);
        datafoto = getResources().obtainTypedArray(R.array.foto);
    }

    private void addItem(){
        heroes = new ArrayList<>();
        for (int i = 0; i < dataname.length; i++){
            Movie mov= new Movie();
            mov.setFoto(datafoto.getResourceId(i, -1));
            mov.setNama(dataname[i]);
            mov.setTahun(datatahun[i]);
            heroes.add(mov);
        }
        adapter.setMovies(heroes);
    }



}
