package com.example.coba1;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MovieDetail extends AppCompatActivity {

    TypedArray dataPhoto;
    ImageView imageMovie;
    String[] dataTitle, dataYears, dataReview, data;
    TextView namaMovie, descMovie, rev, det;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        int asd = intent.getIntExtra("asd",0);

        Log.d("TESTING___",String.valueOf(asd));

        dataTitle = getResources().getStringArray(R.array.movie);
        dataYears = getResources().getStringArray(R.array.desc);
        dataReview = getResources().getStringArray(R.array.review);
        data = getResources().getStringArray(R.array.detail);
        dataPhoto = getResources().obtainTypedArray(R.array.foto);

        imageMovie = findViewById(R.id.img);
        namaMovie = findViewById(R.id.judul);
        descMovie = findViewById(R.id.overview);
        rev = findViewById(R.id.review);
        det = findViewById(R.id.detail);

        namaMovie.setText(dataTitle[asd]);
        descMovie.setText(dataYears[asd]);
        rev.setText(dataReview[asd]);
        det.setText(data[asd]);
        imageMovie.setImageResource(dataPhoto.getResourceId(asd, -1));
    }

}
